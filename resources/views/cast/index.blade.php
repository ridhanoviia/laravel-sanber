@extends('master')

@section('sidebar')
      <!-- SidebarSearch Form -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
@endsection

@section('main_content')
<div class="ml-4 mt-2 mr-4"> 
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Cast Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success')}}
                </div>
            @endif
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 10px">#</th>
                <th style="width: 35%; text-align: center">Nama</th>
                <th style="width: 50px">Umur</th>
                <th style="text-align: center">Bio</th>
                <th style="text-align: center">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($cast as $key => $cast)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td><a href="/cast/{{$cast->id}}"> {{ $cast -> nama }} </td>
                    <td> {{ $cast -> umur }} </td>
                    <td> {{ $cast -> bio }} </td>
                    <td style="display:flex;">
                        <a href="/cast/{{$cast->id}}" class="btn btn-info btn-sm">show</a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-default btn-sm">edit</a> 
                        <form action="/cast/{{$cast->id}}" method="post">
                        {{ csrf_field() }}
                        @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form> 
                    </td>       
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
    <!-- /.card -->
</div>
@endsection