@extends('master')

@section('sidebar')
      <!-- SidebarSearch Form -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
@endsection

@section('main_content')
<div class="ml-4 mt-2 mr-4"> 
    <div class="card">
        <div class="card-header">
        <h3 class="card-title">Show Cast Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <table class="table table-bordered">
            <thead>
            <tr>
                <th style="width: 35%; text-align: center">Nama</th>
                <th style="width: 50px">Umur</th>
                <th style="text-align: center">Bio</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td> {{ $cast -> nama }} </td>
                    <td> {{ $cast -> umur }} </td>
                    <td> {{ $cast -> bio }} </td>
                </tr>
            </tbody>
        </table>
        </div>
    </div>
    <!-- /.card -->
</div>
@endsection