@extends('master')

@section('sidebar')
      <!-- SidebarSearch Form -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">
              <i class="nav-icon far fa-plus-square"></i>
              <p>
                Cast
                <i class="fas fa-angle-left right"></i>
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
@endsection

@section('main_content')
<div class="ml-4 mt-2 mr-4"> 
    <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Cast</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/cast" method="POST">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Cast">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="umur">Umur</label>
            <input type="text" class="form-control" id="umur" name="umur" placeholder="Umur Cast">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name="bio" placeholder="Bio">
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
    <!-- /.card -->
</div>

@endsection